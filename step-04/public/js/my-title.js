Vue.component(`my-title`, {
  template: `
  <div>
    <h1 class="title">
      {{title}}
    </h1> 
  </div>
  `,
  data() {
    return {
      title: "Kitchen Sink 🍛 [Vue.js]",
    }
  },
  methods:{

  },
  mounted() {
    console.log("⚡️ the vue is mounted")
    
    this.$root.$on("ping", (message)=> {
      this.title = `🎉 Tada!!! from ${message}`
    })
  }
});



