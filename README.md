# 🇫🇷 découverte rapide des vue components

Les vue components sont un mode de développement avec Vue.js très intuitif, et il est tout à fait possible de s'y essayer sans aucun tooling.

Ce projet et ses issues vont vous guider pour apprendre une petite (mais importante) partie de Vue.js

## Pour commencer, allez à l'issue [Quoi, Pourquoi, Pour qui, ...](#1)

## Plan

- [Quoi, Pourquoi, Pour qui, ...](#1)
- [Installer un serveur http et préparer le terrain](#2)
- [Préparer votre page index.html](#3)
- [Premier composant](#4)
- [Structurer](#5)
- [Une liste](#6)
- [Faire communiquer les composants](#7)
- [Evaluation finale](#8)

