
Vue.component(`my-list`, {
  template: `
    <div>
      <hr><h2 class="subtitle">{{title}}</h2><hr>
      <div v-for="buddy in buddies">
        <h2 class="subtitle">{{buddy}}</h2>
      </div>
    </div>
  `,
  data() {
    return {
      title: "✨ Buddies List",
      buddies: null
    }
  },
  methods: {
    populateTheList: function() {
      this.buddies = [
        "🦊 Foxy",
        "🦁 Leo",
        "🐯 Tigrou"
      ]
    }
  },
  mounted() {
    console.log("⚡️ the vue is mounted")
    this.populateTheList()
  }
})

